provider "aws" {
  assume_role {
    role_arn     = var.assume_role_arn
    session_name = "test-eks"
  }

  default_tags {
    tags = {
      Owner     = "MickaelCanevet"
      Terraform = "true"
    }
  }
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.eks.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.eks.token
  }
}

provider "argocd" {
  port_forward                = true
  port_forward_with_namespace = helm_release.argocd.namespace
  server_addr                 = "127.0.0.1:8080"
  auth_token                  = jwt_hashed_token.argocd.token
  plain_text                  = true

  kubernetes {
    host                   = data.aws_eks_cluster.eks.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.eks.token
  }
}
