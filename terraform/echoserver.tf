resource "argocd_application" "echoserver" {
  metadata {
    name      = "echoserver"
    namespace = "argocd"
  }

  wait = true

  spec {
    source {
      repo_url        = "https://gitlab.com/mcanevet/test-eks.git"
      path            = "applications/echoserver"
      target_revision = "main"

      helm {
        values = <<-EOT
        ---
        replicaCount: 2
        ingress:
          enabled: true
          annotations:
            kubernetes.io/ingress.class: "alb"
            alb.ingress.kubernetes.io/target-type: "ip"
            alb.ingress.kubernetes.io/healthcheck-port: "8080"
            alb.ingress.kubernetes.io/group.name: ${var.cluster_name}
            alb.ingress.kubernetes.io/certificate-arn: ${var.certificate_arn}
            alb.ingress.kubernetes.io/listen-ports: '[{"HTTP": 80}, {"HTTPS":443}]'
            alb.ingress.kubernetes.io/scheme: "internet-facing"
            alb.ingress.kubernetes.io/ssl-redirect: '443'
            alb.ingress.kubernetes.io/ssl-policy: ELBSecurityPolicy-FS-1-2-Res-2020-10
            alb.ingress.kubernetes.io/tags: Module=mic-poc,Env=poc
            external-dns.alpha.kubernetes.io/ingress-hostname-source: annotation-only
            external-dns.alpha.kubernetes.io/hostname: echoserver.${var.cluster_name}.${var.base_domain}
          hosts:
            - host: echoserver.${var.cluster_name}.${var.base_domain}
              paths:
                - path: /
                  pathType: ImplementationSpecific
            - host: echoserver.${var.base_domain}
              paths:
                - path: /
                  pathType: ImplementationSpecific
          tls:
            - secretName: echoserver-tls
              hosts:
                - echoserver.${var.cluster_name}.${var.base_domain}
                - echoserver.${var.base_domain}
        EOT
      }
    }

    destination {
      server    = "https://kubernetes.default.svc"
      namespace = "echoserver"
    }

    sync_policy {
      automated = {
        prune       = true
        self_heal   = true
        allow_empty = false
      }

      sync_options = ["CreateNamespace=true"]

      retry {
        backoff = {
          duration     = ""
          max_duration = ""
        }
        limit = "0"
      }
    }
  }

  depends_on = [
    argocd_application.aws_load_balancer_controller,
    argocd_application.external_dns,
  ]
}

data "aws_lb" "echoserver" {
  tags = {
    "ingress.k8s.aws/stack" = "eks-poc-blue"
  }

  depends_on = [
    argocd_application.echoserver,
  ]
}

resource "aws_cloudfront_distribution" "echoserver" {
  origin {
    domain_name = data.aws_lb.echoserver.dns_name
    origin_id   = "echoServer"

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "https-only"
      origin_ssl_protocols   = ["TLSv1.2"]
    }
  }

  enabled = true

  aliases = [
    "echoserver.${var.base_domain}",
  ]

  default_cache_behavior {
    allowed_methods          = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods           = ["GET", "HEAD"]
    cache_policy_id          = data.aws_cloudfront_cache_policy.caching_disabled.id
    origin_request_policy_id = data.aws_cloudfront_origin_request_policy.all_viewer.id
    target_origin_id         = "echoServer"
    viewer_protocol_policy   = "redirect-to-https"
  }

  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn      = var.cloudfront_certificate_arn
    minimum_protocol_version = "TLSv1.2_2021"
    ssl_support_method       = "sni-only"
  }
}

resource "aws_route53_record" "echoserver" {
  zone_id = data.aws_route53_zone.this.id
  name    = "echoserver"
  type    = "CNAME"
  ttl     = 300
  records = [aws_cloudfront_distribution.echoserver.domain_name]
}
