data "aws_iam_policy_document" "external_dns" {
  statement {
    actions = [
      "route53:ChangeResourceRecordSets",
    ]

    resources = [
      "arn:aws:route53:::hostedzone/${data.aws_route53_zone.this.id}",
    ]
  }

  statement {
    actions = [
      "route53:ListHostedZones",
      "route53:ListResourceRecordSets",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "external_dns" {
  name   = "ExternalDNSPolicy"
  policy = data.aws_iam_policy_document.external_dns.json
}

module "iam_assumable_role_external_dns" {
  source                        = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version                       = "4.7.0"
  create_role                   = true
  number_of_role_policy_arns    = 1
  role_name                     = format("external-dns-%s", var.cluster_name)
  provider_url                  = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns              = [aws_iam_policy.external_dns.arn]
  oidc_fully_qualified_subjects = ["system:serviceaccount:kube-system:external-dns"]
}

resource "argocd_application" "external_dns" {
  metadata {
    name      = "external-dns"
    namespace = "argocd"
  }

  wait = true

  spec {
    source {
      repo_url        = "https://gitlab.com/mcanevet/test-eks.git"
      path            = "applications/external-dns"
      target_revision = "main"

      helm {
        values = <<-EOT
        ---
        external-dns:
          policy: sync
          txtOwnerId: ${var.cluster_name}
          serviceAccount:
            annotations:
              eks.amazonaws.com/role-arn: ${module.iam_assumable_role_external_dns.iam_role_arn}
        EOT
      }
    }

    destination {
      server    = "https://kubernetes.default.svc"
      namespace = "kube-system"
    }

    sync_policy {
      automated = {
        prune       = true
        self_heal   = true
        allow_empty = false
      }

      retry {
        backoff = {
          duration     = ""
          max_duration = ""
        }
        limit = "0"
      }
    }
  }

  depends_on = [
    argocd_application.argocd,
    module.iam_assumable_role_external_dns,
  ]
}
