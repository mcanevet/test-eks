output "kubeconfig" {
  description = "The content of the KUBECONFIG file."
  value       = module.eks.kubeconfig
  sensitive   = true
}

output "argocd_server_admin_password" {
  description = "The ArgoCD admin password."
  value       = random_password.argocd_server_admin.result
  sensitive   = true
}
