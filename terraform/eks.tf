data "aws_eks_cluster" "eks" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "eks" {
  name = module.eks.cluster_id
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.23.0"

  cluster_version  = "1.21"
  cluster_name     = var.cluster_name
  vpc_id           = module.vpc.vpc_id
  subnets          = module.vpc.private_subnets
  enable_irsa      = true
  write_kubeconfig = false

  cluster_endpoint_public_access_cidrs = ["0.0.0.0/0"]

  node_groups_defaults = {
    ami_type      = "BOTTLEROCKET_x86_64"
    capacity_type = "SPOT"
  }

  node_groups = {
    default = {
      instance_types   = ["t3a.medium"]
      asg_max_size     = 3
      desired_capacity = 2
    }
  }

  workers_additional_policies = [
    "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController", # Required for SecurityGroup for Pods
    "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore",   # Required for Systems Manager
  ]

  depends_on = [
    module.vpc,
  ]
}
