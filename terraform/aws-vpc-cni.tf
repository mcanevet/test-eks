resource "argocd_application" "aws_vpc_cni" {
  metadata {
    name      = "aws-vpc-cni"
    namespace = "argocd"
  }

  wait = true

  spec {
    source {
      repo_url        = "https://gitlab.com/mcanevet/test-eks.git"
      path            = "applications/aws-vpc-cni"
      target_revision = "main"
    }

    destination {
      server    = "https://kubernetes.default.svc"
      namespace = "kube-system"
    }

    sync_policy {
      automated = {
        prune       = true
        self_heal   = true
        allow_empty = false
      }

      retry {
        backoff = {
          duration     = ""
          max_duration = ""
        }
        limit = "0"
      }
    }
  }

  depends_on = [
    argocd_application.argocd,
  ]
}
