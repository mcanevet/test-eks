data "http" "aws_load_balancer_controller_policy_document" {
  url = "https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.3.0/docs/install/iam_policy.json"
}

resource "aws_iam_policy" "aws_load_balancer_controller" {
  name   = "AWSLoadBalancerControllerIAMPolicy"
  policy = data.http.aws_load_balancer_controller_policy_document.body
}

module "iam_assumable_role_aws_load_balancer_controller" {
  source                        = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version                       = "4.7.0"
  create_role                   = true
  number_of_role_policy_arns    = 1
  role_name                     = format("aws-load-balancer-controller-%s", var.cluster_name)
  provider_url                  = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns              = [aws_iam_policy.aws_load_balancer_controller.arn]
  oidc_fully_qualified_subjects = ["system:serviceaccount:kube-system:aws-load-balancer-controller"]
}

resource "argocd_application" "aws_load_balancer_controller" {
  metadata {
    name      = "aws-load-balancer-controller"
    namespace = "argocd"
  }

  wait = true

  spec {
    source {
      repo_url        = "https://gitlab.com/mcanevet/test-eks.git"
      path            = "applications/aws-load-balancer-controller"
      target_revision = "main"

      helm {
        values = <<-EOT
        ---
        aws-load-balancer-controller:
          clusterName: ${var.cluster_name}
          serviceAccount:
            annotations:
              eks.amazonaws.com/role-arn: ${module.iam_assumable_role_aws_load_balancer_controller.iam_role_arn}
        EOT
      }
    }

    destination {
      server    = "https://kubernetes.default.svc"
      namespace = "kube-system"
    }

    sync_policy {
      automated = {
        prune       = true
        self_heal   = true
        allow_empty = false
      }

      retry {
        backoff = {
          duration     = ""
          max_duration = ""
        }
        limit = "0"
      }
    }
  }

  depends_on = [
    argocd_application.argocd,
    module.iam_assumable_role_aws_load_balancer_controller,
  ]
}
