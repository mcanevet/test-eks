variable "base_domain" {
  type = string
}

variable "cluster_name" {
  type    = string
  default = "eks-poc-blue"
}

variable "assume_role_arn" {
  type = string
}

variable "certificate_arn" {
  type = string
}

variable "cloudfront_certificate_arn" {
  type = string
}
