terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    htpasswd = {
      source = "loafoe/htpasswd"
    }
    argocd = {
      source = "oboukili/argocd"
    }
    jwt = {
      source = "camptocamp/jwt"
    }
  }
}
