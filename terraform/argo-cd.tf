locals {
  argocd_chart = yamldecode(file("${path.module}/../applications/argo-cd/Chart.yaml")).dependencies.0

  argocd_accounts_terraform_tokens = jsonencode(
    [
      {
        id  = random_uuid.jti.result
        iat = time_static.iat.unix
      }
    ]
  )

  argocd_values = <<-EOT
    ---
    configs:
      secret:
        argocdServerAdminPassword: "${htpasswd_password.argocd_server_admin.bcrypt}"
        argocdServerAdminPasswordMtime: '2020-07-23T11:31:23Z'
        extra:
          accounts.terraform.tokens: |
            ${local.argocd_accounts_terraform_tokens}
          server.secretkey: "${random_password.argocd_server_secretkey.result}"
    server:
      config:
        accounts.terraform: apiKey
      extraArgs:
        - --insecure
      rbacConfig:
        policy.csv: |
          g, terraform, role:admin
    EOT
}

resource "random_password" "argocd_server_admin" {
  length  = 16
  special = false
}

resource "htpasswd_password" "argocd_server_admin" {
  password = random_password.argocd_server_admin.result
}

resource "random_password" "argocd_server_secretkey" {
  length  = 32
  special = false
}

resource "time_static" "iat" {}

resource "random_uuid" "jti" {}

resource "jwt_hashed_token" "argocd" {
  algorithm = "HS256"
  secret    = random_password.argocd_server_secretkey.result

  claims_json = jsonencode({
    jti = random_uuid.jti.result
    iat = time_static.iat.unix
    iss = "argocd"
    nbf = time_static.iat.unix
    sub = "terraform"
    }
  )
}

resource "helm_release" "argocd" {
  name       = "argo-cd"
  repository = local.argocd_chart.repository
  chart      = "argo-cd"
  version    = local.argocd_chart.version

  namespace         = "argocd"
  dependency_update = true
  create_namespace  = true
  timeout           = 10800

  values = [local.argocd_values]

  depends_on = [
    module.eks,
  ]
}

resource "argocd_application" "argocd" {
  metadata {
    name      = "argo-cd"
    namespace = "argocd"
  }

  wait = true

  spec {
    source {
      repo_url        = "https://gitlab.com/mcanevet/test-eks.git"
      path            = "applications/argo-cd"
      target_revision = "main"

      helm {
        values = yamlencode({ "argo-cd" = yamldecode(local.argocd_values) })
      }
    }

    destination {
      server    = "https://kubernetes.default.svc"
      namespace = "argocd"
    }

    sync_policy {
      automated = {
        prune       = true
        self_heal   = true
        allow_empty = false
      }

      retry {
        backoff = {
          duration     = ""
          max_duration = ""
        }
        limit = "0"
      }
    }
  }

  depends_on = [
    helm_release.argocd,
  ]
}

# NOTE: managing ArgoCD's Ingress separated from the Helm chart so that the
# Ingress is detroyed before aws-load-balancer-controller so that the ALB is
# properly released on destroy
resource "kubernetes_ingress" "argo_cd_argocd_server" {
  metadata {
    name      = "argo-cd-argocd-server"
    namespace = "argocd"

    annotations = {
      "kubernetes.io/ingress.class"                              = "alb"
      "alb.ingress.kubernetes.io/target-type"                    = "ip"
      "alb.ingress.kubernetes.io/healthcheck-port"               = "8080"
      "alb.ingress.kubernetes.io/certificate-arn"                = var.certificate_arn
      "alb.ingress.kubernetes.io/group.name"                     = var.cluster_name
      "alb.ingress.kubernetes.io/listen-ports"                   = "[{\"HTTP\": 80}, {\"HTTPS\":443}]"
      "alb.ingress.kubernetes.io/scheme"                         = "internet-facing"
      "alb.ingress.kubernetes.io/ssl-redirect"                   = 443
      "alb.ingress.kubernetes.io/ssl-policy"                     = "ELBSecurityPolicy-FS-1-2-Res-2020-10"
      "alb.ingress.kubernetes.io/tags"                           = "Module=mic-poc,Env=poc"
      "external-dns.alpha.kubernetes.io/ingress-hostname-source" = "annotation-only"
      "external-dns.alpha.kubernetes.io/hostname"                = "argocd.${var.cluster_name}.${var.base_domain}"
      # "alb.ingress.kubernetes.io/backend-protocol" = "HTTPS"
      # "alb.ingress.kubernetes.io/conditions.argogrpc" = "[{\"field\":\"http-header\",\"httpHeaderConfig\":{\"httpHeaderName\":\"Content-Type\",\"values\":[\"application/grpc\"]}}]"
    }
  }

  spec {
    rule {
      host = "argocd.${var.cluster_name}.${var.base_domain}"
      http {
        # path {
        #   backend {
        #     service_name = "argocdgrpc"
        #     service_port = "http"
        #   }
        # }

        path {
          backend {
            service_name = "argo-cd-argocd-server"
            service_port = "http"
          }
        }
      }
    }

    tls {
      hosts       = ["argocd.${var.cluster_name}.${var.base_domain}"]
      secret_name = "argocd-tls"
    }
  }

  depends_on = [
    argocd_application.aws_load_balancer_controller,
    argocd_application.external_dns,
  ]
}
