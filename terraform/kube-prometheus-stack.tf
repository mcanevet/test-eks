resource "argocd_application" "kube-prometheus-stack" {
  metadata {
    name      = "kube-prometheus-stack"
    namespace = "argocd"
  }

  # wait = true

  spec {
    source {
      repo_url        = "https://gitlab.com/mcanevet/test-eks.git"
      path            = "applications/kube-prometheus-stack"
      target_revision = "main"
    }

    destination {
      server    = "https://kubernetes.default.svc"
      namespace = "kube-prometheus-stack"
    }

    sync_policy {
      automated = {
        prune       = true
        self_heal   = true
        allow_empty = false
      }

      sync_options = ["CreateNamespace=true"]

      retry {
        backoff = {
          duration     = ""
          max_duration = ""
        }
        limit = "0"
      }
    }
  }

  depends_on = [
    argocd_application.aws_load_balancer_controller,
    argocd_application.external_dns,
  ]
}
